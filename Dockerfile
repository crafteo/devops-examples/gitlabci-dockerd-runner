# Dummy Dockerfile emulating a long-running build if cache is not re-used
FROM alpine

RUN echo "A long step which takes a long time without Docker cache" > /msg && sleep 60
RUN echo "Typically when building your app or downloading dependencies..." > /msg2 